# RelayShieldDriver#



### What is this repository for? ###

* This small project helps you to interact with relays. Its for the Raspberry Pi or equivalent


### How do I get set up? ###

* You have to set the *RelayDriver.py* as import.

* A small example you can find here: *test.py*

### Contribution guidelines ###

 * Please contact me for new ideas and opportunities to expand the project