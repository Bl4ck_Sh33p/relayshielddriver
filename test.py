#!/usr/bin/env python3

from src.RelayDriver import *

Relay1 = Relay(7)    # Creates a relay object which uses pin 7
Relay1.setup()
Relay1.switchOn()    # Turns the relay on
Relay1.switchOff()   # Turns the relay off

Relay2 = Relay(8)    # Creates a second relay object
Relay2.setup()
Relay2.switchFor(3)  # Turns the relay for 3 seconds on