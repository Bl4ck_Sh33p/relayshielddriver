#!/usr/bin/env python3

import RPi.GPIO as GPIO
from time import sleep


class Relay():
    # Defines the GPIO pin
    def __init__(self, pin):
        self.state = 0
        self.pin = pin

    # Sets the pin to output
    def setup(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.pin, GPIO.OUT)

    # Turns the relay on
    def switchOn(self):
        self.state = 1
        GPIO.output(self.pin, GPIO.HIGH)

    # Turns the relay off
    def switchOff(self):
        self.state = 0
        GPIO.output(self.pin, GPIO.LOW)

    # Turns the relay for a given time on
    def switchFor(self, time):
        self.switchOn()
        sleep(time)
        self.switchOff()

    # Returns the status of the relay-(Object)
    def getState(self):
        return self.statev